<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Process\PhpProcess;

$running = [];

$readFile = function (string $filename) {
    $script = <<<'EOF'
        <?php
        echo file_get_contents($_SERVER['file']);
        EOF
    ;
    $process = new PhpProcess($script, env: ['file' => $filename]);

    $process->start(function ($type, $data) {
        if ($type === 'out') {
            var_dump($data);
            Fiber::suspend();
        }
    });

    while ($process->isRunning()) {
        Fiber::suspend();
    }
};

$writeFile = function (string $filename, string $data, ?callable $callback = null) {
    $script = <<<'EOF'
        <?php
        file_put_contents($_SERVER['file'], $_SERVER['data']);
        EOF
    ;
    $process = new PhpProcess($script, env: ['file' => $filename, 'data' => $data]);
    $process->start();

    while ($process->isRunning()) {
        Fiber::suspend();
    }

    if ($callback) {
        $callback();
    }
};

$running[] = $fiber = new Fiber($readFile);
$fiber->start('/data/data/test1.txt');

$running[] = $fiber = new Fiber($readFile);
$fiber->start('/data/data/test2.txt');

$running[] = $fiber = new Fiber($writeFile);
$fiber->start('/data/data/test3.txt', 'this is a test of async writing!', function () {
    var_dump('done-zo!');
});

$running[] = $fiber = new Fiber(function () {
    foreach (range(1, 3) as $item) {
        var_dump('junk ' . $item);
        $start = time();
        $end = $start + 1;
        while (time() < $end) {
            Fiber::suspend();
        }
    }
});
$fiber->start();

while (count($running) > 0) {
    $stillRunning = [];
    foreach ($running as $fiber) {
        if (!$fiber->isTerminated()) {
            $stillRunning[] = $fiber;
            $fiber->resume();
        }
    }
    $running = $stillRunning;
}
