<?php

class User
{

}

interface SaveResult
{
    public function accept(SaveVisitor $visitor);
}

interface SaveVisitor
{
    public function visit(SaveResult $result);
    public function visitSuccess(SaveSucceeded $result);
    public function visitFailed(SaveFailed $result);
}

class SaveSucceeded implements SaveResult
{
    public function accept(SaveVisitor $visitor)
    {
        $visitor->visitSuccess($this);
    }
}

class SaveFailed implements SaveResult
{
    public function accept(SaveVisitor $visitor)
    {
        $visitor->visitFailed($this);
    }
}

class Repo
{
    public function save(User $user): SaveSucceeded|SaveFailed
    {
        return new SaveSucceeded();
    }
}

$repo = new Repo();
$user = new User();

$result = $repo->save($user);
$a = new class() implements SaveVisitor {
    public function visit(SaveResult $result)
    {
        $result->accept($this);
    }

    public function visitSuccess(SaveSucceeded $result)
    {
        var_dump('Succeeded!!');
    }

    public function visitFailed(SaveFailed $result)
    {
        var_dump('Failed!!');
    }
};
$a->visit($result);
