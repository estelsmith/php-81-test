FROM alpine:edge AS composer
ARG COMPOSER_VERSION=2.2.9

RUN wget https://getcomposer.org/download/${COMPOSER_VERSION}/composer.phar -O /composer

# ---

FROM alpine:edge

COPY ./docker/etc/apk/repositories /etc/apk/repositories

RUN apk --no-cache add php81 php81-phar php81-iconv php81-mbstring php81-openssl php81-curl php81-dom php81-pecl-mcrypt
RUN ln -s /usr/bin/php81 /usr/local/bin/php

COPY --from=composer /composer /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer
WORKDIR "/data"
