<?php

namespace test;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\ValidatorBuilder;

require __DIR__ . '/vendor/autoload.php';

class UserForm
{
    public function __construct(
        #[NotBlank]
        private ?string $username = null,
        #[NotBlank]
        private ?string $firstName = null,
        #[NotBlank]
        private ?string $lastName = null
    )
    {
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }
}

$validator = Validation::createValidatorBuilder()
    ->enableAnnotationMapping()
    ->getValidator();

$request = [
    'username' => 'estel.smith',
    'firstName' => 'Estel',
//    'lastName' => 'Smith'
];

$user = new UserForm(...$request);

$errors = $validator->validate($user);

if ($errors->count() > 0) {
    dump((string)$errors);
}
