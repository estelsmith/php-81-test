<?php

/**
 * @return ArrayObject
 */
function di_container(): ArrayObject
{
    static $instance = null;

    if (!$instance) {
        $instance = new ArrayObject();
    }

    return $instance;
}

/**
 * @template T
 * @param class-string<T> $parameter
 * @return T
 */
function di_get(string $parameter)
{
    $di = di_container();

    if ($di->offsetExists($parameter)) {
        $service = $di[$parameter];

        if (is_callable($service)) {
            return $service($di);
        }

        return $service;
    }

    return null;
}

/**
 * @param callable $callback
 * @return callable
 */
function di_service(callable $callback): callable
{
    return function () use (&$callback) {
        static $cache = null;

        if (!$cache) {
            $cache = $callback();
        }

        return $cache;
    };
}
