<?php

namespace test;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactoryBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ValidatorBuilder;

require __DIR__ . '/vendor/autoload.php';

class TestForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, [
            'constraints' => [
                new NotBlank()
            ]
        ]);

        $builder->add('password', TextType::class, [
            'constraints' => [
                new NotBlank()
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}

// Lol, stupid simple DI container using nothing but an array.
$di = di_container();
$di[ValidatorBuilder::class] = fn() => Validation::createValidatorBuilder();
$di[ValidatorInterface::class] = di_service(function () {
    return di_get(ValidatorBuilder::class)->getValidator();
});
$di[ValidatorExtension::class] = di_service(function () {
    return new ValidatorExtension(di_get(ValidatorInterface::class));
});
$di[FormFactoryBuilderInterface::class] = fn() => Forms::createFormFactoryBuilder();
$di[FormFactoryInterface::class] = di_service(function () {
    return di_get(FormFactoryBuilderInterface::class)
        ->addExtension(di_get(ValidatorExtension::class))
        ->getFormFactory();
});

$request = [
    'username' => 'test',
    'password' => 'test'
];

$formFactory = di_get(FormFactoryInterface::class);

$form = $formFactory->create(TestForm::class);
$form->submit($request);

dump($form->isValid());
dump($form->getData());
