<?php

require __DIR__ . '/vendor/autoload.php';

interface Action
{
    public function execute();
}

enum UserLevel: string
{
    case ADMIN = 'admin';
    case USER = 'user';
}

interface ActionAttribute
{
    public function execute();
}

#[Attribute]
class RequireAuth implements ActionAttribute
{
    public function __construct(
        public readonly UserLevel $userLevel = UserLevel::USER
    )
    {

    }

    public function execute()
    {
        dump('require auth ' . $this->userLevel->value . '!');
    }
}

#[Attribute]
class TransformInput implements ActionAttribute
{
    public function execute()
    {
        dump('transform input!');
    }
}

class UserListAction implements Action
{
    #[RequireAuth(UserLevel::ADMIN)]
    #[TransformInput]
    public function execute()
    {
        dump('list users!');
    }
}

class ActionHandler
{
    public function handle(Action $action)
    {
        $reflection = new \ReflectionObject($action);
        $method = $reflection->getMethod('execute');

        foreach ($method->getAttributes() as $attribute) {
            $attributeReflection = new \ReflectionClass($attribute->getName());
            if ($attributeReflection->implementsInterface(ActionAttribute::class)) {
                /** @var ActionAttribute $attributeInstance */
                $attributeInstance = $attributeReflection->newInstance(...$attribute->getArguments());
                $attributeInstance->execute();
            }
        }

        $action->execute();
    }
}

$handler = new ActionHandler();
$listAction = new UserListAction();

$handler->handle($listAction);
