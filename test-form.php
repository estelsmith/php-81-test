<?php

namespace test;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Validation;

require __DIR__ . '/vendor/autoload.php';

class AddressForm
{
    private ?string $address = null;
    private ?string $city = null;

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }
}

class UserForm
{
    private ?string $username = null;
    private ?AddressForm $address = null;

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    public function getAddress(): ?AddressForm
    {
        return $this->address;
    }

    public function setAddress(?AddressForm $address): void
    {
        $this->address = $address;
    }
}

class AddressFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('address', TextType::class, [
            'constraints' => [
                new NotBlank()
            ]
        ]);
        $builder->add('city', TextType::class, [
            'constraints' => [
                new NotBlank()
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
//            'data_class' => AddressForm::class
        ]);
    }
}

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, [
            'constraints' => [
                new NotBlank()
            ]
        ]);
        $builder->add('address', AddressFormType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
//            'data_class' => UserForm::class
        ]);
    }
}

$validator = Validation::createValidatorBuilder()
    ->enableAnnotationMapping()
    ->getValidator();

$formFactory = Forms::createFormFactoryBuilder()
    ->addExtension(new ValidatorExtension($validator))
    ->getFormFactory();

$request = [
    'username' => 'test',
    'address' => null
];

$form = $formFactory->create(UserFormType::class);
$form->submit($request);
if (!$form->isValid()) {
    dump('invalid');
    foreach ($form->getErrors(true) as $name => $error) {
        dump($error->getOrigin()->getName(), $error->getMessage());
    }
}

dump($form->getData());
